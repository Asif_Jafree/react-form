import React from 'react';
import './App.css';
import Form from './components/Form';
import Navbar from './components/Navbar';
import About from './components/About';
import Home from './components/Home';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

class App extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Switch>
            <Router exact path="/Form">
              <Form />
            </Router>
            <Router exact path="/About">
              <About />
            </Router>
            <Router exact path="/">
              <Home />
            </Router>
          </Switch>
        </div>
      </Router>
    );
  }
}
export default App;
