import React from 'react';
import './Style.css';
export default class About extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="about">
        <h2>About freeCodeCamp </h2>
        <img
          src="https://www.freecodecamp.org/news/content/images/size/w2000/2020/01/freecodecamp.jpeg"
          alt="freeCodeCampImage"
        ></img>
        <h3>What is freeCodeCamp?</h3>
        <p>
          We’re a nonprofit community that helps you learn to code by building
          projects.
        </p>
        <h3>How can you help me learn to code?</h3>
        <p>
          You'll learn to code by completing coding challenges and building
          projects. You'll also earn verified certifications along the way.
        </p>
      </div>
    );
  }
}
