import React from 'react';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="home">
        <h4>Learn to code — for free.</h4>
        <h4>Build projects.</h4>
        <h4>Earn certifications.</h4>
        <p>
          Since 2014, more than 40,000 freeCodeCamp.org graduates have gotten
          jobs at tech companies.
        </p>
      </div>
    );
  }
}
