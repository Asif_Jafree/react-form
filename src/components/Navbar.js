import React from 'react';
import { Link } from 'react-router-dom';
import './Style.css';

class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="navbar">
        <Link to="/" className="nav">
          Home
        </Link>
        <Link to="/Form" className="nav">
          SignUp
        </Link>
        <Link to="/About" className="nav">
          About
        </Link>
      </div>
    );
  }
}
export default Navbar;
