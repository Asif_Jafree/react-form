import React from 'react';
import validator from 'validator';
export default class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      age: '',
      password: '',
      confirmPassword: '',
      checkbox: false,
      checkName: '',
      checkEmail: '',
      checkAge: '',
      checkPassword: '',
      checkConfirmPassword: '',
      checkCheckbox: '',
      submit: false,
    };
  }

  handleChangeEvent = (e) => {
    let comingValue = e.target.value;
    if (e.target.name === 'checkbox') {
      comingValue = e.target.checked;
    }
    this.setState({ [e.target.name]: comingValue });
  };

  submitFunction = (e) => {
    e.preventDefault();

    let obj = {
      checkName: '',
      checkEmail: '',
      checkAge: '',
      checkPassword: '',
      checkConfirmPassword: '',
      checkCheckbox: '',
      submit: true,
    };

    const {
      name: validName,
      email: validEmail,
      age: validAge,
      password: validPassword,
      confirmPassword: validConfirmPassword,
    } = this.state;

    //name validation
    if (validator.isEmpty(validName)) {
      obj.checkName = '*Please enter your name';
      obj.submit = false;
    } else if (!validator.isAlpha(validName, 'en-US', { ignore: ' ' })) {
      obj.checkName = '*Please enter only alphabet and space';
      obj.submit = false;
    } else if (!validator.isLength(validName, { min: 2, max: undefined })) {
      obj.checkName = '*Name should have atleast 2 letter';
      obj.submit = false;
    }
    //email validation

    if (validator.isEmpty(validEmail)) {
      obj.checkEmail = '*Please enter your email';
      obj.submit = false;
    } else if (!validator.isEmail(validEmail)) {
      obj.checkEmail = '*Please enter correct email e.g xyz@example.com';
      obj.submit = false;
    }

    //age validate

    if (validator.isEmpty(validAge)) {
      obj.checkAge = '*Please enter your age';
      obj.submit = false;
    } else if (
      !validator.isInt(validAge, {
        gt: 0,
        lt: 101,
        allow_leading_zeroes: false,
      })
    ) {
      obj.checkAge = '*Enter a valid age between 1 to 100';
      obj.submit = false;
    }

    //password validate

    if (validator.isEmpty(validPassword)) {
      obj.checkPassword = '*Please enter your password';
      obj.submit = false;
    } else if (
      !validator.isStrongPassword(validPassword, {
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1,
      })
    ) {
      obj.checkPassword =
        '*Password must have eight charector(atleast 1-lowerCase 1-UpperCase and 1-number)';
      obj.submit = false;
    }

    //confirmPassword validate

    if (validator.isEmpty(validConfirmPassword)) {
      obj.checkConfirmPassword = '*Please confirm you password';
      obj.submit = false;
    } else if (validConfirmPassword !== validPassword) {
      obj.checkConfirmPassword = '*Your password is not matched';
      obj.submit = false;
    }

    //checkbox validation

    if (this.state.checkbox === false) {
      obj.checkCheckbox = '*Please accept the terms and condition';
      obj.submit = false;
    }
    this.setState(obj);
  };

  render() {
    return (
      <div className="root">
        {this.state.submit ? (
          <div className="result">
            <h1>Form Submitted Sucessfully</h1>
            <div className="resultValue">Name : {this.state.name}</div>
            <div className="resultValue">Age : {this.state.age}</div>
            <div className="resultValue">Email:{this.state.email}</div>
          </div>
        ) : (
          <div className="major">
            <h1>Sign Up</h1>

            <div>{this.state.message}</div>

            <form className="form">
              <label className="lable">Name</label>
              <input
                className="name"
                name="name"
                type="text"
                placeholder="Enter Your Name"
                required
                onChange={this.handleChangeEvent}
              ></input>
              <div className="errorMessage">{this.state.checkName}</div>

              <label className="label">Email</label>
              <input
                className="name"
                type="email"
                id="email"
                name="email"
                placeholder="Enter Your email"
                required
                onChange={this.handleChangeEvent}
              ></input>
              <div className="errorMessage">{this.state.checkEmail}</div>

              <label className="label">Age</label>
              <input
                className="name"
                type="number"
                name="age"
                placeholder="Enter your Age"
                required
                onChange={this.handleChangeEvent}
              ></input>
              <div className="errorMessage">{this.state.checkAge}</div>

              <label className="label">Password</label>
              <input
                className="name"
                type="password"
                name="password"
                placeholder="Enter Password"
                required
                onChange={this.handleChangeEvent}
              ></input>
              <div className="errorMessage">{this.state.checkPassword}</div>

              <label className="label">Confirm Password</label>
              <input
                className="name"
                type="password"
                name="confirmPassword"
                placeholder="Re-Enter Password"
                required
                onChange={this.handleChangeEvent}
              ></input>
              <div className="errorMessage">
                {this.state.checkConfirmPassword}
              </div>
              <div className="checkbox">
                <input
                  type="checkbox"
                  id="check"
                  name="checkbox"
                  required
                  onChange={this.handleChangeEvent}
                ></input>
                <label className="label" for="check">
                  I read all the terms and condition{' '}
                </label>
                <div className="errorMessage">{this.state.checkCheckbox}</div>
              </div>

              <button className="btn" onClick={this.submitFunction}>
                Submit
              </button>
            </form>
          </div>
        )}
      </div>
    );
  }
}
